﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMSOFT.EasyHotel.DAL.Models
{
    [PetaPoco.TableName("Bill")]
    [PetaPoco.PrimaryKey("BillId", autoIncrement = true)]
  public  class Bill
    {
        public long BillId { get; set; }
        public String BillNo { get; set; }
        public int RoomId { get; set; }
        public short IsHourRoom{ get; set; } //是否是钟点房
        public DateTime? CheckinDate { get; set; }
        public DateTime? CheckoutDate { get; set; }
        public int StayTotalDays { get; set; }
        public int StayTotalHours { get; set; }
        public decimal SinglePrice { get; set; }//房间单价
        public decimal MinFeeForHourRoom { get; set; }//钟点房最低消费
        public decimal PricePerHour { get; set; }//钟点房每小时费用
        public decimal RoomTotalFee { get; set; } //房间总费用
        public decimal ConsumeFee { get; set; } //消费金额
        public string ConsumeItem { get; set; }
        public decimal Deposit { get; set; } //押金
        public int TravelAgencyId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerIDNo { get; set; }
        public int CreatedByUserId { get; set; }
        public int UpdatedByUserId { get; set; }
        public int PayTypeId { get; set; }
        public int BillStatus { get; set; }
        private DateTime _created = DateTime.Now;
        public DateTime Created { get { return _created; } set { _created = value; } }
        private DateTime _modified = DateTime.Now;
        public DateTime Modified { get { return _modified; } set { _modified = value; } }
        public string Comments { get; set; }
    }
}
