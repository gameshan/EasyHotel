﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMSOFT.EasyHotel.DAL.Models
{
    [PetaPoco.TableName("User")]
    [PetaPoco.PrimaryKey("UserId",autoIncrement=true)]
    public class User
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string PassWord { get; set; }
        public string FullName { get; set; }
        public int GroupHotelId { get; set; }
        public int SubHotelId { get; set; }
        public int RoleId { get; set; }
        private DateTime _created = DateTime.Now;
        public DateTime Created { get { return _created; } set { _created = value; } }
        private DateTime _modified = DateTime.Now;
        public DateTime Modified { get { return _modified; } set { _modified = value; } }
    }
}
