﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IEMSOFT.EasyHotel.DAL.Models;
namespace IEMSOFT.EasyHotel.DAL.Models.Extension
{
  public  class BillExtension:Bill
    {
      public string TravelAgencyName { get; set; }
      public string PayTypeName { get; set; }
      public string CreateUserFullName { get; set; }
      public string UpdateUserFullName { get; set; }
      public int SubHotelId { get; set; }
      public int RoomTypeId { get; set; }
      public string RoomNo { get; set; }
      public string RoomTypeName { get; set; }
      public string SubHotelName { get; set; }
    }
}
