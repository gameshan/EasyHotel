﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IEMSOFT.EasyHotel.Common;

namespace IEMSOFT.EasyHotel.Admin.Models
{
    public class RoomStatusModel : RoomModel
    {
        public long BillId { get; set; }
        public RoomStatusType RoomStatusId { get; set; }
        public string CustomerTravelAgency { get; set; }
        public string CustomerPayType { get; set; }
        public string CustomerCheckinDate { get; set; }
        public string CustomerDeposit { get; set; }
        public string SinglePrice { get; set; }
        public string BillCreateUserFullName { get; set; }
        public string CustomerName { get; set; }
        public bool IsHourRoom { get; set; }
        public string RoomStatusName
        {
            get
            {
                var ret = "";
                switch (RoomStatusId)
                {
                    case RoomStatusType.Normal:
                        ret = "空闲中";
                        break;
                    case RoomStatusType.CheckedIn:
                        if (IsHourRoom)
                            ret = "钟点房入住";
                        else
                            ret = "每日房入住";
                        break;
                    case RoomStatusType.IsMending:
                        ret = "维修中";
                        break;
                    default:
                        ret = "空闲中";
                        break;
                }
                return ret;
            }
        }
    }
}